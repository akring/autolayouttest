//
//  ViewController.m
//  AutoLayoutTest
//
//  Created by Akring on 15/3/15.
//  Copyright (c) 2015年 Akring. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *secondObjectHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SecondObjectTopSpace;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)show:(id)sender{
    
    self.secondObjectHeight.constant = 107;
    self.SecondObjectTopSpace.constant = 60;
}

- (IBAction)hide:(id)sender{
    
    self.secondObjectHeight.constant = 0;
    self.SecondObjectTopSpace.constant = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
